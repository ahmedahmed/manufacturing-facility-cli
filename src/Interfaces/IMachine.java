/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

/**
 *
 * @author Ben Rouha
 */
public interface IMachine {
    String creatMachine(String Name,String Id);
    String addUnits(String id , String nbrUnits );
    String setTemperature(String id , String nbrUnits );
    String getTemperatureByMachineId(String id );
    String totalUnitsProduced();
    String averageUnitsProduced();
}
