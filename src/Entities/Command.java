/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import Interfaces.ICommand;
import Interfaces.IMachine;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Ben Rouha
 */
public class Command implements ICommand{
    private String keyWord;
    private int nbrOfParams;
    private String action;
    private Class[] cArg;
    private List<Command> commandes=new ArrayList<Command>();
    IMachine im = new Machine(); 
    public Command() {
        Class[] cArg2={String.class,String.class};
        Class[] cArg1={String.class};
        commandes.add(new Command("create", 3,"creatMachine",cArg2));
        commandes.add(new Command("add", 3, "addUnits",cArg2));
        commandes.add(new Command("temperature", 2 ,"getTemperatureByMachineId",cArg1));
        commandes.add(new Command("temperature", 3,"setTemperature",cArg2));
        commandes.add(new Command("total", 1,"totalUnitsProduced",null));
        commandes.add(new Command("average", 1,"averageUnitsProduced",null));
    }
   
    public Command(String keyWord, int nbrOfParams, String action,Class[] carg) {
        this.keyWord = keyWord;
        this.nbrOfParams = nbrOfParams;
        this.action = action;
        this.cArg = carg;
    }
    
    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public int getNbrOfParams() {
        return nbrOfParams;
    }

    public void setNbrOfParams(int nbrOfParams) {
        this.nbrOfParams = nbrOfParams;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public List<Command> getCommandes() {
        return commandes;
    }

    public Class[] getArgs() {
        return cArg;
    }

    public void setArgs(Class[] args) {
        this.cArg = args;
    }

    @Override
    public String manageCommand(String input) {
        Command command = new Command();
        String[] ary = input.split(" ");
        Command c;
        try {
            c = command.getCommandes().stream()
                .filter(x->x.getKeyWord().equals(ary[0].toString()) && x.getNbrOfParams()==ary.length)
                .findFirst()
                .get();
        } catch (Exception e) {
            c = null;
        }
        
        if(c == null)
        return "wrong command";
        else{
            try {
                Method method;
                method = im.getClass().getMethod(c.getAction(), c.getArgs());
                try {
                    if(ary.length==3)
                    return (method.invoke(im,ary[1],ary[2])).toString();
                    if(ary.length==2)
                    return (method.invoke(im,ary[1])).toString();
                    else
                    return (method.invoke(im)).toString();
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    return "Error !!";
                }
            } catch (NoSuchMethodException | SecurityException ex) {
                return "Error !!";
            }
        }
    }
    
}
