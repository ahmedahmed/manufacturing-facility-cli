/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import Interfaces.IMachine;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ben Rouha
 */
public class Machine implements IMachine{
    private String id;
    private String name;
    private int temperature;
    private int nbrOfProducedUnits;
    private static List<Machine> machines=new ArrayList<Machine>();

    public Machine() {
    }

    public Machine(String id, String name) {
        this.id = id;
        this.name = name;
        this.temperature = 0;
        this.nbrOfProducedUnits =0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    @Override
    public String creatMachine(String Name, String id) {
        int result;
        result = machines.stream()
                .filter(x->x.id.equals(id))
                .toArray().length;
        if(result == 0) {
            Machine m = new Machine(id, Name);
            machines.add(m);
            return "A machine Was Added successfully !";
        }else{
            return "Sry but that id was already used !";
        }
    }
    public int findMachineIndexById(String id) {
        return machines.indexOf(machines.stream()
                .filter(x->x.id.equals(id))
                .findFirst().get());
    }
    @Override
    public String addUnits(String id, String nbrUnits) {
        try {
            int temp = Integer.parseInt(nbrUnits);
        } catch (Exception e) {
            return "the number of units must be a number !";
        }
        try {
            if(Integer.parseInt(nbrUnits)<0)
                return "the number of units must be a positive number !";
            machines.get(findMachineIndexById(id)).nbrOfProducedUnits+=Integer.parseInt(nbrUnits);
            return nbrUnits+" units were produced by machine id : "+id;
        } catch (Exception e) {
            return "no such machine id found";
        }
       
    }

    @Override
    public String setTemperature(String id, String temperature) {
        try {
            int temp = Integer.parseInt(temperature);
        } catch (Exception e) {
            return "temperature must be a number !";
        }
        try {
            machines.get(findMachineIndexById(id)).temperature=Integer.parseInt(temperature);
            return "the temperature was set to "+temperature+"° for machine id : "+id;
        } catch (Exception e) {
            return "no such machine id found";
        }
    }

    @Override
    public String getTemperatureByMachineId(String id) {
        try {
            int temperature = machines.get(findMachineIndexById(id)).temperature;
            return "the temperature of machine id : "+id+" is : " + temperature+"°";
        } catch (Exception e) {
            return "no such machine id found";
        }
    }

    @Override
    public String totalUnitsProduced() {
        int result=0;
        for (Machine machine : machines) {
            result+=machine.nbrOfProducedUnits;
        }
        return String.valueOf(result);
    }

    @Override
    public String averageUnitsProduced() {
        int nbrOfMachines = machines.size();
        if(nbrOfMachines == 0)
            return "Average : "+nbrOfMachines;
        else{
        int total = Integer.parseInt(totalUnitsProduced());
        return String.valueOf(total/nbrOfMachines);
        }
    }
}
