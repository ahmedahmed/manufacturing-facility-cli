/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package technicalassignment;

import Entities.Command;
import Interfaces.ICommand;
import java.util.Scanner;


/**
 *
 * @author Ben Rouha
 */
public class TechnicalAssignment {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ICommand IC = new Command();
        while(true){
        System.out.print("facility_console> ");
        Scanner scanner = new Scanner(System.in);
        String myCommand = scanner.nextLine();
        System.out.println(IC.manageCommand(myCommand));
        }
    }
    
}
