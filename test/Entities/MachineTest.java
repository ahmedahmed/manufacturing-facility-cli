/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ben Rouha
 */
public class MachineTest {
    
    public MachineTest() {
        String id = "a";
        String Name = "a";
        Machine instance = new Machine();
        String result = instance.creatMachine(id, Name);
    }

    /**
     * Test of creatMachine method, of class Machine.
     */
    @Test
    public void testCreatMachine() {
        System.out.println("creatMachine");
        String id = "a2";
        String Name = "a";
        Machine instance = new Machine();
        String expResult = "A machine Was Added successfully !";
        String result = instance.creatMachine(id, Name);
        assertEquals(expResult, result);
    }

    /**
     * Test of findMachineIndexById method, of class Machine.
     */
    @Test
    public void testFindMachineIndexById() {
        System.out.println("findMachineIndexById");
        String id = "a";
        Machine instance = new Machine();
        int expResult = 0;
        int result = instance.findMachineIndexById(id);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of addUnits method, of class Machine.
     */
    @Test
    public void testAddUnits() {
        System.out.println("addUnits");
        String id = "a";
        String nbrUnits = "50";
        Machine instance = new Machine();
        String expResult = "50 units were produced by machine id : a";
        String result = instance.addUnits(id, nbrUnits);
        assertEquals(expResult, result);
   
    }

    /**
     * Test of setTemperature method, of class Machine.
     */
    @Test
    public void testSetTemperature_String_String() {
        System.out.println("setTemperature");
        String id = "a3";
        String temperature = "60";
        Machine instance = new Machine();
        String expResult = "no such machine id found";
        String result = instance.setTemperature(id, temperature);
        assertEquals(expResult, result);
    }

    /**
     * Test of getTemperatureByMachineId method, of class Machine.
     */
    @Test
    public void testGetTemperatureByMachineId() {
        System.out.println("getTemperatureByMachineId");
        String id = "a3";
        Machine instance = new Machine();
        String expResult = "no such machine id found";
        String result = instance.getTemperatureByMachineId(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of totalUnitsProduced method, of class Machine.
     */
    @Test
    public void testTotalUnitsProduced() {
        System.out.println("totalUnitsProduced");
        Machine instance = new Machine();
        String expResult = "0";
        String result = instance.totalUnitsProduced();
        assertEquals(expResult, result);
    }

    /**
     * Test of averageUnitsProduced method, of class Machine.
     */
    @Test
    public void testAverageUnitsProduced() {
        System.out.println("averageUnitsProduced");
        Machine instance = new Machine();
        String expResult = "25";
        String result = instance.averageUnitsProduced();
        assertEquals(expResult, result);
    }
    
}
