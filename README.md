manufacturing facility cli
Console application : 
in order to make an application that user can interact with using commands i suggested creating a class Command where each object have
-KeyWord , examples: create, add , temperature ,... 
-number of params, examples: create m1 id1 (3params), temperature id1 (2params) 
-the name of the function that will be invoked, examples: creatMachine,addUnits,...
-the params types of the function that will be invoked, example:[String,Integer]

the machines will be stored in a list of type machine and all the action will be on that list add, search, edit, ... 

How to run the application :
run this command 'java -jar TechnicalAssignment.jar'

How to use the appplication : 5 possible commands anything else the programme will return this error message 'wrong command'

to create a machine: 
-create machine_id machine_name
to add units to a machine the units nbr must be a positive number : 
-add machine_id units_nbr
to set the temperature of a machine
-temperature machine_id temperature
to get the temperature of a machine
-temperature machine_id
to get the total number of produced units
-total
to get the average number of produced units
-average

*the machine id is unique 